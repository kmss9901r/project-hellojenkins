pipeline {
    agent any

    environment {
        VERSION = sh(
                returnStdout: true,
                script: "grep version package.json | awk '{print \$2}' | sed  's/[\"|,]//g'"
            )
        REGISTRY_URL = 'registry.gitlab.com'
        REGISTRY_PROJ = 'kmss9901r/project-hellojenkins'
    }

    stages {
        stage('build') {
            steps {
                sh 'echo "Building.., app version: $VERSION"'
                sh 'docker build --no-cache -t $JOB_NAME:$BUILD_NUMBER .'
            }
        }

        stage('test') {
            parallel {
                stage('unit_test') {
                    steps {
                        echo 'Running unit tests..'
                        sh 'docker run --rm $JOB_NAME:$BUILD_NUMBER /bin/sh -c "npm install --include=dev && npm test"'
                    }
                }

                stage('api_test') {
                    steps {
                        echo 'Running api tests..'
                        sh 'docker run --rm $JOB_NAME:$BUILD_NUMBER /bin/sh -c "npm install --include=dev && npm run testapi"'
                    }
                }

            }
        }

        stage('publish') {
            when {
                expression { env.GIT_BRANCH == 'origin/main' }
            }
            steps {
                echo 'Publishing image..'
                echo 'it is me2'
                withCredentials([usernamePassword(credentialsId: 'gitlab-project-hellojenkins-token', usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_TOKEN')]) {
                    echo 'it is me'
                    sh 'echo $REGISTRY_TOKEN | docker login -u $REGISTRY_USER --password-stdin $REGISTRY_URL'
                    sh 'docker tag $JOB_NAME:$BUILD_NUMBER $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                    sh 'docker push $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                }
            }
        }
    }

    post {
        cleanup {
            echo 'Cleanup..'
            script {
                if (env.GIT_BRANCH == 'origin/main') {
                    sh 'docker rmi $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                }
                sh 'docker rmi $JOB_NAME:$BUILD_NUMBER'
            }
        }
    }
}
